require 'rails_helper'

RSpec.describe "models/new", :type => :view do
  before(:each) do
    assign(:model, Model.new(
      :brand_id => 1,
      :name => "MyString",
      :photo => "MyString"
    ))
  end

  it "renders new model form" do
    render

    assert_select "form[action=?][method=?]", models_path, "post" do

      assert_select "input#model_brand_id[name=?]", "model[brand_id]"

      assert_select "input#model_name[name=?]", "model[name]"

      assert_select "input#model_photo[name=?]", "model[photo]"
    end
  end
end
