class Model < ActiveRecord::Base
  belongs_to :brand
  mount_uploader :photo, PhotoUploader
end
