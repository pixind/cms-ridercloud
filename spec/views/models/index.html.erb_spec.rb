require 'rails_helper'

RSpec.describe "models/index", :type => :view do
  before(:each) do
    assign(:models, [
      Model.create!(
        :brand_id => 1,
        :name => "Name",
        :photo => "Photo"
      ),
      Model.create!(
        :brand_id => 1,
        :name => "Name",
        :photo => "Photo"
      )
    ])
  end

  it "renders a list of models" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Photo".to_s, :count => 2
  end
end
