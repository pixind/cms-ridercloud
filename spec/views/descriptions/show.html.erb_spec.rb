require 'rails_helper'

RSpec.describe "descriptions/show", :type => :view do
  before(:each) do
    @description = assign(:description, Description.create!(
      :brand_id => 1,
      :model_id => 2,
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/MyText/)
  end
end
