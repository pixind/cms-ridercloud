require 'rails_helper'

RSpec.describe "descriptions/new", :type => :view do
  before(:each) do
    assign(:description, Description.new(
      :brand_id => 1,
      :model_id => 1,
      :description => "MyText"
    ))
  end

  it "renders new description form" do
    render

    assert_select "form[action=?][method=?]", descriptions_path, "post" do

      assert_select "input#description_brand_id[name=?]", "description[brand_id]"

      assert_select "input#description_model_id[name=?]", "description[model_id]"

      assert_select "textarea#description_description[name=?]", "description[description]"
    end
  end
end
