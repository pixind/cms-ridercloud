module ApplicationHelper
  def title(value)
    unless value.nil?
      @title = "#{value} | Dtscms"      
    end
  end
end
