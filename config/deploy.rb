#require "bundler/capistrano"

load "config/recipes/loads"

server "baku.pixind.net", :web, :app, :db, primary: true
ssh_options[:paranoid] = false
default_run_options[:pty] = true
ssh_options[:forward_agent] = true

##
# Base Config
set :deploy_via, :copy
set :repository,  "/Users/txarli/projects/dtscms"
set :user, "pixind"
set :runner, "pixind"
set :application, "dtscms"
set :domain, "cms.ridercloud.com"
set :deploy_to, "/home/#{user}/#{application}"
set :use_sudo, false
set :scm, "git"
set :maintenance_template_path, File.expand_path("../recipes/templates/maintenance.html.erb", __FILE__)
set :default_environment, { 'PATH' => "/home/#{user}/.rbenv/shims/:$PATH" }

##
# Monit Config
set :alert_email, 'info@pixind.com'
set :monit_mailserver, "smtp.gmail.com"
set :monit_mailserver_port, 587
set :monit_mail_username, "mailer@pixind.com" 
set :monit_mail_password, "Ah9jeLLCC6xCPWECpmb4pNGREgFC7xYA"
set :monit_admin_password, "koshinage"

after "deploy", "deploy:cleanup"