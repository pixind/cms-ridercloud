load "config/recipes/base"
#load "config/recipes/postgresql"
load "config/recipes/nginx"
load "config/recipes/unicorn"
#load "config/recipes/check"
load "config/recipes/monit"
load "config/recipes/shared"
load "config/recipes/rbenv"
load "config/recipes/log"