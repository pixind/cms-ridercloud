include ActionView::Helpers::SanitizeHelper

class Description < ActiveRecord::Base

  belongs_to :brand
  belongs_to :model

  # before_save :strip_html
  def strip_html # Automatically strips any tags from any string to text typed column
    self.description = strip_tags(description)
  end

end
