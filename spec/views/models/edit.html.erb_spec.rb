require 'rails_helper'

RSpec.describe "models/edit", :type => :view do
  before(:each) do
    @model = assign(:model, Model.create!(
      :brand_id => 1,
      :name => "MyString",
      :photo => "MyString"
    ))
  end

  it "renders the edit model form" do
    render

    assert_select "form[action=?][method=?]", model_path(@model), "post" do

      assert_select "input#model_brand_id[name=?]", "model[brand_id]"

      assert_select "input#model_name[name=?]", "model[name]"

      assert_select "input#model_photo[name=?]", "model[photo]"
    end
  end
end
