json.array!(@descriptions) do |description|
  json.extract! description, :id, :brand_id, :model_id, :description
  json.url description_url(description, format: :json)
end
