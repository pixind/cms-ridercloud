require 'rails_helper'

RSpec.describe "models/show", :type => :view do
  before(:each) do
    @model = assign(:model, Model.create!(
      :brand_id => 1,
      :name => "Name",
      :photo => "Photo"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Photo/)
  end
end
