namespace :shared do

  desc "Symlink shared directories"
  task :symlink, roles: :app do
    run "ln -nfs #{shared_path}/public/media  #{release_path}/public/media"
  end
  after "postgresql:symlink", "shared:symlink"

end