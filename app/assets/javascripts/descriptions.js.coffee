# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#
# Full Button Opts
#["bold", "italic", "underline", "strikeThrough",
# "fontSize", "fontFamily", "color", "sep",
# "formatBlock", "blockStyle", "align",
# "insertOrderedList", "insertUnorderedList",
# "outdent", "indent", "sep", "createLink",
# "insertImage", "insertVideo",
# "insertHorizontalRule", "undo", "redo",
# "html"]

$ ->
  $("#edit").editable({inlineMode: false, imageUpload: false, buttons: ["bold", "italic", "underline", "strikeThrough",
                                                                        "fontSize", "fontFamily", "color", "sep",
                                                                        "formatBlock", "blockStyle", "align",
                                                                        "insertOrderedList", "insertUnorderedList",
                                                                        "outdent", "indent", "sep",
                                                                        "insertHorizontalRule", "undo", "redo",
                                                                        "html"] })
  return