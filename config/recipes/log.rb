namespace :log do

  desc "Install log rotate script"
  task :setup, :roles => :app do
    logrotate = <<-BASH 
      #{shared_path}/log/*.log {
        daily
        missingok
        rotate 30
        compress
        size 5M
        delaycompress
        sharedscripts
        postrotate
          service unicorn_#{application} restart
        endscript
      }
    BASH
    tmpfile = "/tmp/#{application}.logrotate"

    put(logrotate, tmpfile)
    run "#{sudo} chown root:root #{tmpfile} && #{sudo} mv -f #{tmpfile} /etc/logrotate.d/#{application}"
  end
end