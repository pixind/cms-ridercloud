class CreateDescriptions < ActiveRecord::Migration
  def change
    create_table :descriptions do |t|
      t.integer :brand_id
      t.integer :model_id
      t.text :description

      t.timestamps
    end
  end
end
