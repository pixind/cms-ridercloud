require 'rails_helper'

RSpec.describe "descriptions/index", :type => :view do
  before(:each) do
    assign(:descriptions, [
      Description.create!(
        :brand_id => 1,
        :model_id => 2,
        :description => "MyText"
      ),
      Description.create!(
        :brand_id => 1,
        :model_id => 2,
        :description => "MyText"
      )
    ])
  end

  it "renders a list of descriptions" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
