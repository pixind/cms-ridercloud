class AddProcessedToDescriptions < ActiveRecord::Migration
  def change
    add_column :descriptions, :processed, :boolean, default: false
  end
end
